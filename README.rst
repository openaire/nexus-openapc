**************************
OpenAIRE MONITOR - OpenAPC
**************************

Bielefeld University Library contribution to the OpenAIRE NEXUS project
phase (01/2021-06/2023).

* CoRDIS `<https://cordis.europa.eu/project/id/101017452>`_   ; `<https://doi.org/10.3030/101017452>`_

Recording of the public launch event of OpenAIRE NEXUS from

* March, 10.th 2021 at `<https://www.youtube.com/watch?v=poNnpyDfX8s>`_

OpenAPC webinars

* October, 6th 2021

  * post: `<https://www.openaire.eu/item/openapc-making-costs-of-open-access-publishing-transparent-in-the-eosc-2>`_
  * recording: `<https://youtu.be/Ez_nIsoHN1k?t=241>`_
  * slides: `<https://doi.org/10.5281/zenodo.5552910>`_

* April, 4.th 2023 at

  * post: `<https://www.openaire.eu/item/openapc-open-minds-towards-transparent-publication-fees>`_
  * recording: `<https://youtu.be/E5RhSmxCcLc>`_
  * slides: `<https://doi.org/10.5281/zenodo.7802410>`_
  * participants: 171


Created feedback formular for OpenAPC contributors:

* https://docs.google.com/forms/d/1MX3bsGxeFX0yLHJC0HnNPa2OE7ep5rt9RIaXIxNvScQ/edit


Roadmap shared via Trello:

* https://trello.com/b/fOLZeGuH/nexus-openapc-public-roadmap ​

Project directory at Bielefeld University Library at SCIEBO

  `<https://uni-bielefeld.sciebo.de/apps/files/?dir=/openaire/infraeosc-07-a3_openaire-nexus>`_

