####################
Virtual Access Costs
####################

.. contents::

Monitoring of KPI
~~~~~~~~~~~~~~~~~

One component of the VAC is to monitor/oberserve the performance of the service (APCs). In the case of OpenAPC are these

* Contributing institutions

But also number of interest could be

* aggregated amount
* count

each for APCs, BPCs and should be enhance with transformative agreements.


ICM provide information about the fetching of metrics via `Prometheus <https://issue.openaire.research-infrastructures.eu/projects/openaire/wiki/Prometheus>`_.
Is oriented by `OpenMetrics.io <https://openmetrics.io>`_.


Example Prometheus related
~~~~~~~~~~~~~~~~~~~~~~~~~~

From the ticket and the "text format example" from https://prometheus.io/docs/instrumenting/exposition_formats/#text-format-example

.. code-block:: text

    # HELP openapc_version_info OpenAIRE-NEXUS OpenAPC prometheus client on 2021-04-22T12:23:39+02:00 (= 1619087019)
    # TYPE openapc_version_info gauge
    openapc_version_info{buildhost="openaire.ub.uni-bielefeld.de",version="0.0.1"} 1.0
    # HELP openapc_contributing_institutions_total overall number of contributing organizations/institutions to the Article and Book Processing Charges
    # TYPE openapc_contributing_institutions_total counter
    openapc_contributing_institutions_total 296
    # HELP openapc_nexus_contributing_institutions_total total number of contributing organizations/institutions to the Article and Book Processing Charges since NEXUS proposal
    # TYPE openapc_nexus_contributing_institutions_total counter
    openapc_nexus_contributing_institutions_total 36
    # HELP openapc_apc_contributing_institutions_total total number of contributing organizations/institutions to the Article Processing Charges
    # TYPE openapc_apc_contributing_institutions_total counter
    openapc_apc_contributing_institutions_total 287
    # HELP openapc_nexus_apc_contributing_institutions_total total number of contributing organizations/institutions to the Article Processing Charges since NEXUS proposal
    # TYPE openapc_nexus_apc_contributing_institutions_total counter
    openapc_nexus_apc_contributing_institutions_total 27
    # HELP openapc_apc_aggregated_amount_total total amount of all Article Processing Charges contributions (currency)
    # TYPE openapc_apc_aggregated_amount_total counter
    openapc_apc_aggregated_amount_total(currency="EUR") 239455318
    # HELP openapc_apc_count_total total total number of all Article Processing Charge records
    # TYPE openapc_apc_count_total counter
    openapc_apc_count_total 123137
    # HELP openapc_bpc_contributing_institutions_total total number of contributing organizations/institutions to the Book/Monograph Processing Charges
    # TYPE openapc_bpc_contributing_institutions_total counter
    openapc_bpc_contributing_institutions_total 9
    # HELP openapc_nexus_bpc_contributing_institutions_total total number of contributing organizations/institutions to the Book Processing Charges since NEXUS proposal
    # TYPE openapc_nexus_bpc_contributing_institutions_total counter
    openapc_nexus_bpc_contributing_institutions_total 9
    # HELP openapc_bpc_aggregated_amount_total total amount of all Book Processing Charges contributions (currency)
    # TYPE openapc_bpc_aggregated_amount_total counter
    openapc_bpc_aggregated_amount_total(currency="EUR") 7669857
    # HELP openapc_bpc_count_total total number of all Book Processing Charge records
    # TYPE openapc_bpc_count_total counter
    openapc_bpc_count_total 1239


outdated formats below
~~~~~~~~~~~~~~~~~~~~~~

structued JSON format (`RFC 8259 <https://tools.ietf.org/html/rfc8259>`_) and OpenMetrics style suggestions from https://docs.google.com/document/d/1ZjyKiKxZV83VI9ZKAXRGKaUKK2BIWCT7oiGBKDBpjEY/edit 
(old versions):


.. code-block:: json

   [
       {
            "baselabels": { 
                "__name__": "apc_contributing_institutions"
            },
            "docstring": "The count of contributing Instiutions to APCs from OpenAPC project",
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 282
                        }
                ]
            }
        },
        {
            "baselabels": { 
                "__name__": "apc_aggregated_sum"
            },
            "docstring": "The overall APC sum from OpenAPC project (EUR)",
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 233237624
                        }
                ]
            }
        },
        {
            "baselabels": { 
                "__name__": "apc_count"
            },
            "docstring": "The APC artcile count from OpenAPC project",
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 119269
                        }
                ]
            }
        },        
        {
            "baselabels": { 
                "__name__": "bpc_contributing_institutions"
            },
            "docstring": "The count of contributing Instiutions to APCs from OpenAPC project"
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 6
                        }
                ]
            }
        },
        {
            "baselabels": { 
                "__name__": "bpc_aggregated_sum"
            },
            "docstring": "The overall BPC sum from OpenAPC project (EUR)"
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 7491859
                        }
                ]
            }
        },
        {
            "baselabels": { 
                "__name__": "bpc_count"
            },
            "docstring": "The BPC artcile count from OpenAPC project"
            "metric": {
                "type": "counter",
                "value": [
                        {
                            "labels": {
                            },
                            "value": 1210
                        }
                ]
            }
        }
    ]        



Example not Prometheus related
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: json

   {
    "service": "OpenAPC",
    "datetime": "2021-03-16T10:31:46+01:00",
    "required": [
        "apc",
        "bpc"
    ],
    "apc": {
        "contributingInstitutions": 282,
        "aggregatedSum": {
            "amount": 233237624,
            "currency": "EUR"
        },
        "count": 119269
    },
    "bpc": {
        "contributingInstitutions": 6,
        "aggregatedSum": { 
            "amount": 7491859,
            "currency": "EUR" 
        },
        "count": 1210 
    }
   }




alternative format

.. code-block:: json

   {
    "service": "OpenAPC",
    "datetime": "2021-03-16T10:31:46+01:00",
    "required": [
        "contributingInstitutions",
        "aggregatedSum",
        "count"
    ],
    "contributingInstitutions": {
       "apc": 282, 
       "bpc": 6 
    },
    "aggregatedSum": { 
       "apc": {
            "amount": 233237624,
            "currency": "EUR"
        },
       "bpc": 7491859 
    },
    "count": { 
       "apc": 119269, 
       "bpc": 1210 
    }
   }


The related ticket "Provide units of access information for OpenAPC" in WP3 is `#6415 <https://issue.openaire.research-infrastructures.eu/issues/6415>`_ 
