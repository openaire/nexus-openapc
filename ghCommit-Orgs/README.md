# OpenAPC - organizational KPI

KPI defined and monitored in deliverables 5.1 - 5.3 during the OpenAIRE Nexus phase.

* [D5.1](https://doi.org/10.5281/zenodo.6559969)
* [D5.2](-)
* [D5.3](-)

**KPI-Name**: DataProviderInstitutions (Unit of Access)
**KPI-Definition**: Number of institutions providing the data

The contributions of new organizations are listed for 
* APC in nexus_apc-2021-23e.csv
* BPC in nexus_bpc-2021-23e.csv

Both files provides information on
* commitdate , the [GitHub commit date and time](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-github-profile/managing-contribution-settings-on-your-profile/troubleshooting-commits-on-your-timeline)
* commitid, the GitHub identifier with link during the commit to the repository
* currentnumber, the current sequence number in the OpenAPC project
* nexusnumber, the Nexus phase sequence number
* indicator, if the organization is _inserted_ or _removed_
* organization, the organization name

During one commit, more than one organizations could insert/removed. Only the overall
number is stored in the column *nexusnumber*.


