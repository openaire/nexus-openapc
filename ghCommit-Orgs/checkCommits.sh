#!/bin/bash
#
# check which institution are joined OpenAPC at which datetime and commit
#
# 2023-03-10 ; Andreas Czerniak ; initial
#

# git log --merges --after=2020-12-16 2>&1 | tee -a commitsMerges_20230310-20201217.log 
# grep 'commit ' commitsMerges_20230310-20201217.log | sed 's#commit ##' | tac > commitsMerges_202012-20230310.log
#FILECOMMITS=`cat commitsMerges_202012-20230412_20230412r.log` # commitsMerges_202012-20230403_comm.log` # commitsMerges_202012-20230310.log`
#FILECOMMITS=`cat commitsMerges_202012-20230504_20230504r.log` # commitsMerges_202012-20230403_comm.log` # commitsMerges_202012-20230310.log`
#FILECOMMITS=`cat commitsMerges_202012-20230607_II.log`
#FILECOMMITS=`cat commitsMerges_202012-20230626.log`
FILECOMMITS=`cat commitsMerges_20201217-20230703.csv`

MYPATH=`pwd`

rm ${MYPATH}/nexus_apc-2021-23.csv
touch ${MYPATH}/nexus_apc-2021-23.csv
rm ${MYPATH}/apc_orgs.csv
touch ${MYPATH}/apc_orgs.csv

rm ${MYPATH}/nexus_bpc-2021-23.csv
touch ${MYPATH}/nexus_bpc-2021-23.csv
rm ${MYPATH}/bpc_orgs.csv
touch ${MYPATH}/bpc_orgs.csv

#cd ../openapc-de_20230412
#cd ../openapc-de_20230504-1120
#cd ../openapc-de_20230607-0830
#cd ../openapc-de_20230626-2200
# latest git repository ; archived as openapc-de_20230703-0823.tar.xz
cd ../openapc-de_20230703-0823

for c in ${FILECOMMITS}; do
  
  git checkout $c
  COMMITDATE=`git log --merges --date=iso8601-strict | head -n 4 | grep -i 'Date: ' | sed 's#Date:   ##'`
  csvcut -c 1 data/apc_de.csv  | grep -v '^institution$' | sort -u > ${MYPATH}/data/apc_orgs_${COMMITDATE}.csv
  csvcut -c 1 data/bpc.csv  | grep -v '^institution$' | sort -u > ${MYPATH}/data/bpc_orgs_${COMMITDATE}.csv

  NUMBER=`cat ${MYPATH}/data/apc_orgs_${COMMITDATE}.csv | wc -l`
  BPCNUMBER=`cat ${MYPATH}/data/bpc_orgs_${COMMITDATE}.csv | wc -l`
  echo $NUMBER , $BPCNUMBER
  ORGDIFF=`diff ${MYPATH}/apc_orgs.csv ${MYPATH}/data/apc_orgs_${COMMITDATE}.csv`
  BPCORGDIFF=`diff ${MYPATH}/bpc_orgs.csv ${MYPATH}/data/bpc_orgs_${COMMITDATE}.csv`

  if [ -n "$ORGDIFF" ] ; then
    echo ${COMMITDATE},${c},${NUMBER},${ORGDIFF} >> ${MYPATH}/nexus_apc-2021-23.csv
  fi
  if [ -n "$BPCORGDIFF" ] ; then
    echo ${COMMITDATE},${c},${BPCNUMBER},${BPCORGDIFF} >> ${MYPATH}/nexus_bpc-2021-23.csv
  fi
  cp ${MYPATH}/data/apc_orgs_${COMMITDATE}.csv ${MYPATH}/apc_orgs.csv
  cp ${MYPATH}/data/bpc_orgs_${COMMITDATE}.csv ${MYPATH}/bpc_orgs.csv

done

cd ${MYPATH}
