#!/bin/bash
#
# check which institution are joined OpenAPC at which datetime and commit
#
# 2023-03-10 ; Andreas Czerniak ; initial
#

# git log --merges --after=2020-12-16 2>&1 | tee -a commitsMerges_20230310-20201217.log 
# grep 'commit ' commitsMerges_20230310-20201217.log | sed 's#commit ##' | tac > commitsMerges_202012-20230310.log
#FILECOMMITS=`cat commitsMerges_202012-20230412_20230412r.log` # commitsMerges_202012-20230403_comm.log` # commitsMerges_202012-20230310.log`
#FILECOMMITS=`cat commitsMerges_202012-20230504_20230504r.log` # commitsMerges_202012-20230403_comm.log` # commitsMerges_202012-20230310.log`
#FILECOMMITS=`cat commitsMerges_202012-20230607_II.log`
#FILECOMMITS=`cat commitsMerges_202012-20230626.log`
FILECOMMITS=`cat commitsMerges_20201217-20230703.csv`

BASEAPC=272
BASEBPC=5

MYPATH=`pwd`

rm ${MYPATH}/nexus_apc-2021-23e.csv
touch ${MYPATH}/nexus_apc-2021-23e.csv
rm ${MYPATH}/apc_orgse.csv
touch ${MYPATH}/apc_orgse.csv

rm ${MYPATH}/nexus_bpc-2021-23e.csv
touch ${MYPATH}/nexus_bpc-2021-23e.csv
rm ${MYPATH}/bpc_orgse.csv
touch ${MYPATH}/bpc_orgse.csv

#cd ../openapc-de_20230412
#cd ../openapc-de_20230504-1120
#cd ../openapc-de_20230607-0830
#cd ../openapc-de_20230626-2200
# latest git repository ; archived as openapc-de_20230703-0823.tar.xz
cd ../openapc-de_20230703-0823

echo "commitdate,commitid,currentnumber,nexusnumber,indicator,organization" >> ${MYPATH}/nexus_apc-2021-23e.csv
echo "commitdate,commitid,currentnumber,nexusnumber,indicator,organization" >> ${MYPATH}/nexus_bpc-2021-23e.csv


for c in ${FILECOMMITS}; do
  
  git checkout $c
  COMMITDATE=`git log --merges --date=iso8601-strict | head -n 4 | grep -i 'Date: ' | sed 's#Date:   ##'`
  csvcut -c 1 data/apc_de.csv  | grep -v '^institution$' | sort -u > ${MYPATH}/data/apc_orgs_${COMMITDATE}e.csv
  csvcut -c 1 data/bpc.csv  | grep -v '^institution$' | sort -u > ${MYPATH}/data/bpc_orgs_${COMMITDATE}e.csv

  NUMBER=`cat ${MYPATH}/data/apc_orgs_${COMMITDATE}e.csv | wc -l`
  BPCNUMBER=`cat ${MYPATH}/data/bpc_orgs_${COMMITDATE}e.csv | wc -l`
  echo $NUMBER , $BPCNUMBER
  ORGDIFF=`diff ${MYPATH}/apc_orgse.csv ${MYPATH}/data/apc_orgs_${COMMITDATE}e.csv  | grep '[<>]' | sed 's# #,#' | sed 's# #_#g' `
  BPCORGDIFF=`diff ${MYPATH}/bpc_orgse.csv ${MYPATH}/data/bpc_orgs_${COMMITDATE}e.csv | grep '[<>]' | sed 's# #,#' | sed 's# #_#g' `

  if [ -n "$ORGDIFF" ] ; then
    for chgline in ${ORGDIFF} ; do
#      echo ${chgline}
      MODINDRAW=`echo ${chgline} | csvcut -c1 `
#      echo ${MODINDRAW}
      if [[ "$MODINDRAW" == ">" ]]; then MODIND="inserted" ; fi
      if [[ "$MODINDRAW" == "<" ]]; then MODIND="removed" ; fi
      MODORG=`echo ${chgline} | csvcut -c2 `
      ((NEXUSNUMBER=$NUMBER-$BASEAPC))
#      echo ${MODORG}
      echo ${COMMITDATE},https://github.com/OpenAPC/openapc-de/commit/${c},${NUMBER},${NEXUSNUMBER},${MODIND},${MODORG} >> ${MYPATH}/nexus_apc-2021-23e.csv
    done
  fi
  if [ -n "$BPCORGDIFF" ] ; then
    for chgline in ${BPCORGDIFF} ; do
      MODINDRAW=`echo ${chgline} | csvcut -c1 `
      if [[ "$MODINDRAW" == ">" ]]; then MODIND="inserted" ; fi
      if [[ "$MODINDRAW" == "<" ]]; then MODIND="removed" ; fi
      MODORG=`echo ${chgline} | csvcut -c2 `
      ((NEXUSNUMBER=$BPCNUMBER-$BASEBPC))
      echo ${COMMITDATE},https://github.com/OpenAPC/openapc-de/commit/${c},${BPCNUMBER},${NEXUSNUMBER},${MODIND},${MODORG} >> ${MYPATH}/nexus_bpc-2021-23e.csv
    done
  fi
  cp ${MYPATH}/data/apc_orgs_${COMMITDATE}e.csv ${MYPATH}/apc_orgse.csv
  cp ${MYPATH}/data/bpc_orgs_${COMMITDATE}e.csv ${MYPATH}/bpc_orgse.csv

done

cd ${MYPATH}
