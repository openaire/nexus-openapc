#!/bin/bash
# 
# fetchOpenAPC KPIs diretly from GitHub
#
#
# 2021/03 ; Andreas Czerniak
# 2021/04
# 2023/03 ; Andreas Czerniak ; baseLine discussion and correct numbers based on the GitHub-commits.
#                              The history of each commit during the period is available in the sub-directory ghCommit-Orgs/

APCCONINSTBASE="272" # 260
BPCCONINSTBASE="5"   #   0
ADATE=`date --iso-8601=seconds`
SDATE=`date +%s`
APCCOUNT=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Articles ' | awk '{print $3;}' | sed 's#,##g' | sed 's#|##g' `
BPCCOUNT=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Monographs ' | awk '{print $3;}' | sed 's#,##g' | sed 's#|##g' `
APCCONINST=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Articles ' | awk '{print $7;}' | sed 's#,##g' `
BPCCONINST=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Monographs ' | awk '{print $7;}' | sed 's#,##g' `
APCSSUM=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Articles ' | awk '{print $5;}' | sed 's#,##g' `
BPCSUM=`wget --quiet -O - https://raw.githubusercontent.com/OpenAPC/openapc-de/master/README.md | grep '| Monographs ' | awk '{print $5;}' | sed 's#,##g' `
ONUPTIME=`cat kpi/uptime`
APCNEXUSCONINST=$((APCCONINST-APCCONINSTBASE))
BPCNEXUSCONINST=$((BPCCONINST-BPCCONINSTBASE))

PCCONTRIB=$((APCCONINST+BPCCONINST))
PCNEXUSCONINST=$((PCCONTRIB-APCCONINSTBASE-BPCCONINSTBASE))

# exposing format for Prometheus monitoring: https://prometheus.io/docs/instrumenting/exposition_formats/
## HELP apc_contributing_institutions_total number
## TYPE apc_contributing_institutions_total counter
#apc_contributing_institutions_total 0.0

#echo "# 'type' 'number' 'seconds since 1970'"
#echo "# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds at ${ADATE}."
#echo "# TYPE process_cpu_seconds_total counter"
#echo "process_cpu_seconds_total 0.12"
echo "# "
echo "# HELP openapc_version_info OpenAIRE-NEXUS OpenAPC prometheus client on ${ADATE} (= ${SDATE})"
echo "# TYPE openapc_version_info gauge"
echo "openapc_version_info{buildhost=\"openaire.ub.uni-bielefeld.de\",version=\"1.0\"} 1"
echo "# HELP openapc_contributing_institutions_total overall number of contributing organizations/institutions to the Article and Book Processing Charges"
echo "# TYPE openapc_contributing_institutions_total counter"
echo "openapc_contributing_institutions_total ${PCCONTRIB}"
echo "# HELP openapc_nexus_uptime_total total number of uptime since last update"
echo "# TYPE openapc_nexus_uptime_total counter"
echo "openapc_nexus_uptime_total ${ONUPTIME}"
echo "# HELP openapc_nexus_contributing_institutions_total total number of contributing organizations/institutions to the Article and Book Processing Charges since NEXUS proposal" 
echo "# TYPE openapc_nexus_contributing_institutions_total counter"
echo "openapc_nexus_contributing_institutions_total ${PCNEXUSCONINST}"
echo "# HELP openapc_apc_contributing_institutions_total total number of contributing organizations/institutions to the Article Processing Charges"
echo "# TYPE openapc_apc_contributing_institutions_total counter"
echo "openapc_apc_contributing_institutions_total ${APCCONINST}"
echo "# HELP openapc_nexus_apc_contributing_institutions_total total number of contributing organizations/institutions to the Article Processing Charges since NEXUS proposal"
echo "# TYPE openapc_nexus_apc_contributing_institutions_total counter"
echo "openapc_nexus_apc_contributing_institutions_total ${APCNEXUSCONINST}"
echo "# HELP openapc_apc_aggregated_amount_total total amount of all Article Processing Charges contributions (currency)"
echo "# TYPE openapc_apc_aggregated_amount_total counter"
echo "openapc_apc_aggregated_amount_total{currency=\"EUR\"} ${APCSSUM}"
echo "# HELP openapc_apc_count_total total total number of all Article Processing Charge records"
echo "# TYPE openapc_apc_count_total counter"
echo "openapc_apc_count_total ${APCCOUNT}"
if [ "$1" = "bpc" ] || [ "$1" = "all" ] ; then
  echo "# HELP openapc_bpc_contributing_institutions_total total number of contributing organizations/institutions to the Book/Monograph Processing Charges"
  echo "# TYPE openapc_bpc_contributing_institutions_total counter"
  echo "openapc_bpc_contributing_institutions_total ${BPCCONINST}"
  echo "# HELP openapc_nexus_bpc_contributing_institutions_total total number of contributing organizations/institutions to the Book Processing Charges since NEXUS proposal"
  echo "# TYPE openapc_nexus_bpc_contributing_institutions_total counter"
  echo "openapc_nexus_bpc_contributing_institutions_total ${BPCNEXUSCONINST}"
  echo "# HELP openapc_bpc_aggregated_amount_total total amount of all Book Processing Charges contributions (currency)"
  echo "# TYPE openapc_bpc_aggregated_amount_total counter"
  echo "openapc_bpc_aggregated_amount_total{currency=\"EUR\"} ${BPCSUM}"
  echo "# HELP openapc_bpc_count_total total number of all Book Processing Charge records"
  echo "# TYPE openapc_bpc_count_total counter"
  echo "openapc_bpc_count_total ${BPCCOUNT}"
fi

if [ "$1" = "all" ]; then
echo
echo "== == == =="

echo "{"
echo " \"service\": \"OpenAPC\","
echo " \"datetime\": \"${ADATE}\","
echo " \"required\": ["
echo "     \"apc\","
echo "     \"bpc\""
echo " ],"
echo " \"apc\": {"
echo "     \"contributingInstitutions\": $APCCONINST,"
echo "     \"aggregatedSum\": {"
echo "         \"amount\": $APCSSUM,"
echo "         \"currency\": \"EUR\" "
echo "     },"
echo "     \"count\": $APCCOUNT"
echo " },"
echo " \"bpc\": {"
echo "     \"contributingInstitutions\": $BPCCONINST,"
echo "     \"aggregatedSum\": {"
echo "         \"amount\": $BPCSUM,"
echo "         \"currency\": \"EUR\""
echo "     },"
echo "     \"count\": $BPCCOUNT"
echo " }"
echo "}"
echo
echo "== == =="
echo 
echo "alternative format"
echo
echo "{"
echo "  \"service\": \"OpenAPC\", "
echo "  \"datetime\": \"$ADATE\","
echo "  \"contributingInstitutions\": { "
echo "     \"apc\": $APCCONINST, "
echo "     \"bpc\": $BPCCONINST "
echo "  },"
echo "  \"aggregatedSum\": { "
echo "     \"currency\": \"EUR\", "
echo "     \"apc\": $APCSSUM, "
echo "     \"bpc\": $BPCSUM "
echo "  },"
echo "  \"count\": { "
echo "     \"apc\": $APCCOUNT, "
echo "     \"bpc\": $BPCCOUNT "
echo "  }"
echo "}"
fi

echo "$ADATE,$APCCONINST,$APCSSUM,\"EUR\",$APCCOUNT,$BPCCONINST,$BPCSUM,\"EUR\",$BPCCOUNT" >> kpi_log.csv
